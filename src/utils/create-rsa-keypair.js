#!/usr/bin/env node
'use strict';

const { exec } = require('child_process');


let privateKey = '', publicKey = '';
exec('openssl genrsa -out ./key 4096', function(error, stderr, stdout) {
	if(error) return console.error(error);
	privateKey = stdout;
	console.log('stdout: ' + stdout, 'stderr: ' + stderr);
	exec('openssl rsa -pubout -in ./key -out ./key.pub', function(error2, stderr2, stdout2) {
		if(error2) return console.error(error2);
		publicKey = stdout2;
		console.log('stdout2: ' + stdout2, 'stderr2: ' + stderr2);
	})
});
