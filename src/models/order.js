'use strict';

const mongoose = require('mongoose');
const hr = require('http-responder');

const User = mongoose.model('User');
const ShoppingCart = mongoose.model('ShoppingCart');


const ObjectId = mongoose.Schema.Types.ObjectId;

const OrderSchema = new mongoose.Schema({
	customerID: { type: ObjectId, required: true, unique: true, ref: User },
	cartID: { type: ObjectId, required: true, ref: ShoppingCart },
	finalPrice: { type: Number, default: 0 },
	address: {
		city: { type: String },
		street: { type: String },
		buildingNumber: { type: Number },
		aptNumber: { type: Number },
	},
	orderDate: { type: Date },
	creditCardNumber: { type: Number, required: true, min: 1000, max: 9999 },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
OrderSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});
OrderSchema.methods.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};
OrderSchema.methods.getUser = function() {
	const that = this;
	return User
		.findById(that.customerID);
};
OrderSchema.methods.getCart = function() {
	const that = this;
	return Cart
		.findById(that.customerID);
};

module.exports = mongoose.model('Order', OrderSchema);
