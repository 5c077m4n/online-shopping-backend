module.exports.User = require('./user');
module.exports.Category = require('./category');
module.exports.Product = require('./product');
module.exports.Item = require('./shopping-cart-item');
module.exports.Cart = require('./shopping-cart');
module.exports.Order = require('./order');
