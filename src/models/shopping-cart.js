'use strict';

const mongoose = require('mongoose');
const hr = require('http-responder');


const ObjectId = mongoose.Schema.Types.ObjectId;
const Item = mongoose.model('ShoppingCartItem');

const ShoppingCartSchema = new mongoose.Schema({
	customerID: { type: ObjectId, required: true, unique: true },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});

ShoppingCartSchema.methods.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};
ShoppingCartSchema.methods.getItems = function() {
	const that = this;
	return Item
		.find({ cartID: that._id });
};
ShoppingCartSchema.methods.getItemById = function(itemID) {
	const that = this;
	return Item
		.findOne({ cartID: that._id, _id: itemID });
};

ShoppingCartSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});

module.exports = mongoose.model('ShoppingCart', ShoppingCartSchema);
