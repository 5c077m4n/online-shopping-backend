'use strict';

const mongoose = require('mongoose');
const hr = require('http-responder');


const Category = mongoose.model('Category');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ProductSchema = new mongoose.Schema({
	name: { type: String, required: true },
	categoryID: { type: ObjectId, required: true, ref: Category },
	price: { type: Number, required: true },
	image: { type: String, default: '/assets/images/default.jpg' },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
ProductSchema.methods.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};
ProductSchema.methods.getCategory = function() {
	const that = this;
	return Category
		.findById(that.categoryID);
};
ProductSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});

module.exports = mongoose.model('Product', ProductSchema);
