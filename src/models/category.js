'use strict';

const mongoose = require('mongoose');
const hr = require('http-responder');


const CategorySchema = new mongoose.Schema({
	name: { type: String, required: true, unique: [
		true, 'Sorry, this email is already in use.'
	] },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
CategorySchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});
CategorySchema.methods.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};

module.exports = mongoose.model('Category', CategorySchema);
