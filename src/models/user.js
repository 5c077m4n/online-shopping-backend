'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const hr = require('http-responder');
const to = require('await-fn');

const secret = require('../utils/secret');


const Schema = mongoose.Schema;
const randomNumber = (min, max) =>
	(max >= min)? Math.floor(Math.random() * (max - min) + min) : 0;
const wait = time => new Promise(resolve => setTimeout(resolve, time));

const UserSchema = new Schema({
	firstName: { type: String, trim: true },
	lastName: { type: String, trim: true },
	username: { type: String, required: true, unique: [
		true, 'Sorry, this username is already in use.'
	] },
	email: { type: String, trim: true, required: true, unique: [
		true, 'Sorry, this email is already in use.'
	] },
	address: {
		city: { type: String },
		street: { type: String },
		buildingNumber: { type: Number },
		aptNumber: { type: Number },
	},
	isManager: { type: Boolean, default: false },
	secret: { type: String, required: true, select: false, default: secret },
	password: { type: String, required: true, select: false },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
UserSchema.pre('save', async function(next) {
	const user = this;
	if(!user.password) return next();
	const [err, hash] = await to(bcrypt.hash, {
		params: [user.password, 14]
	});
	if(err) return next(err);
	user.password = hash;
	return next();
});
UserSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});

UserSchema.methods.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};

UserSchema.statics.authenticate = function(username, password, next) {
	if(!username || !password) return next(hr.unauthorized(
		'Both the username and password are required.'
	));
	else return this
		.findOne({ username })
		.select('+password')
		.select('+secret')
		.exec()
		.then(async user =>
			(user)? user :
				await wait(randomNumber(1500, 2000))
					.then(() => Promise.reject(hr.unauthorized(
						'Incorrect username/password inserted.'
					)))
		)
		.then(async user => {
			const [err, same] = await to(bcrypt.compare, {
				params: [password, user.password]
			});
			if(err) return Promise.reject(err);
			if(!same) return Promise.reject(hr.unauthorized(
				'Incorrect username/password inserted.'
			));
			user.password = undefined;
			return user;
		})
		.catch(Promise.reject);
};

module.exports = mongoose.model('User', UserSchema);
