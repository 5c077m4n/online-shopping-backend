'use strict';

const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const hr = require('http-responder');


const ObjectId = mongoose.Schema.Types.ObjectId;

const ShoppingCartItemSchema = new mongoose.Schema({
	productID: { type: ObjectId, required: true, ref: Product },
	cartID: { type: ObjectId, required: true },
	amount: { type: Number, default: 1 },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
ShoppingCartItemSchema.index({ productID: 1, cartID: 1}, { unique: true });

ShoppingCartItemSchema.method.update = function(updates) {
	Object.assign(this, updates, { modifiedAt: new Date() });
	return this.save();
};
ShoppingCartItemSchema.method.getProduct = function() {
	const that = this;
	return Product
		.findById(that.productID);
};
ShoppingCartItemSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error.message, doc));
	else next();
});

module.exports = mongoose.model('ShoppingCartItem', ShoppingCartItemSchema);
