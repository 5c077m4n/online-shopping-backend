'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');

const Cart = mongoose.model('ShoppingCart');
const Item = mongoose.model('ShoppingCartItem');


router.route('/')
	.get((req, res, next) => {
		return Cart
			.findOne({ customerID: req.body.customerID })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post((req, res, next) => {
		return new Cart(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.route('/:cartID')
	.get((req, res, next) => {
		return Cart
			.findById(req.params.cartID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return Cart
			.findById(req.params.cartID)
			.update(req.body)
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Cart
			.findByIdAndRemove(req.params.cartID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/:cartID/items')
	.get((req, res, next) => {
		return Item
			.find({ cartID: req.params.cartID })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post((req, res, next) => {
		req.body.cartID = req.params.cartID;
		return Item
			.findOne({
				cartID: req.body.cartID,
				productID: req.body.productID
			})
			.exec()
			.then(item =>
				(item)? new Item(item)
					.update(req.body)
					.exec()
					.then(data => hr.created(data).end(res))
					.catch(next)
				: new Item(req.body)
					.save()
					.then(data => hr.created(data).end(res))
					.catch(next)
			)
			.catch(next);
	});

router.route('/:cartID/items/count')
	.get((req, res, next) => {
		return Item
			.countDocuments({ cartID: req.params.cartID })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/:cartID/items/:itemID')
	.get((req, res, next) => {
		return Item
			.getItem(req.params.itemID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return Cart
			.findById(req.params.cartID)
			.exec()
			.then(data => new Item(data))
			.then(item => {
				return item
					.getItem(req.params.itemID)
					.exec()
					.update(req.body)
					.then(data => hr.ok(data).end(res))
					.catch(next);
			})
			.catch(next);
	})
	.delete((req, res, next) => {
		return Item
			.findByIdAndRemove(req.params.itemID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/:cartID/items/:itemID/product')
	.get((req, res, next) => {
		return Item
			.findById(req.params.itemID)
			.exec()
			.then(data => new Item(data))
			.then(item => {
				return item
					.getProduct()
					.exec()
					.then(data => hr.ok(data).end(res))
					.catch(next);
			})
			.catch(next);
	});

module.exports = router;
