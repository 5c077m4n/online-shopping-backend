'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');

const Order = mongoose.model('Order');


router.route('/')
	.get((req, res, next) => {
		return Order
			.find({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post((req, res, next) => {
		return new Order(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.route('/count')
	.get((req, res, next) => {
		return Order
			.countDocuments({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/:orderID')
	.get((req, res, next) => {
		return Order
			.findById(req.params.orderID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return Order
			.findById(req.params.orderID)
			.update(req.body)
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Order
			.findByIdAndRemove(req.params.orderID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

module.exports = router;
