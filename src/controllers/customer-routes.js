'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');


const User = mongoose.model('User');

router.route('/')
	.get((req, res, next) => {
		return User
			.find({ isManager: false })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post((req, res, next) => {
		return new User(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.route('/:userID')
	.get((req, res, next) => {
		return User
			.findOne({ _id: req.params.userID, isManager: false })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return User
			.findById(req.params.userID)
			.update(req.body)
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return User
			.findByIdAndRemove(req.params.userID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.use(
	'/:userID/carts',
	(req, res, next) => {
		req.body.customerID = req.params.userID;
		return next();
	},
	require('../controllers/cart-routes')
);

module.exports = router;
