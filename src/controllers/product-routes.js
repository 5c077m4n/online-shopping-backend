'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');

const Product = mongoose.model('Product');
const Category = mongoose.model('Category');
const middelware = require('../config/middleware');


router.route('/')
	.get((req, res, next) => {
		return Product
			.find({})
			.sort({ name: +1 })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post(middelware.isManager, (req, res, next) => {
		return new Product(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.route('/:prodID')
	.get((req, res, next) => {
		return Product
			.findById(req.params.prodID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return Product
			.findById(req.params.prodID)
			.update(req.body)
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Product
			.findByIdAndRemove(req.params.prodID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/:prodID/category')
	.get((req, res, next) => {
		return Product
			.findById(req.params.prodID)
			.exec()
			.then(data => new Product(data))
			.then(prod => {
				return prod
					.getCategory()
					.exec()
					.then(data => hr.ok(data).end(res))
					.catch(next);
			})
			.catch(next);
	})
	.put((req, res, next) => {
		return Product
			.findById(req.params.prodID)
			.update({ categoryID: req.body.categoryID })
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

module.exports = router;
