'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');

const middelware = require('../config/middleware');
const Category = mongoose.model('Category');
const Products = mongoose.model('Product');


router.route('/')
	.get((req, res, next) => {
		return Category
			.find({})
			.sort({ name: +1 })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post(middelware.isManager, (req, res, next) => {
		return new Category(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.route('/:catID')
	.get((req, res, next) => {
		return Category
			.findById(req.params.catID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.delete(middelware.isManager, (req, res, next) => {
		return Category
			.findByIdAndRemove(req.params.catID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

	router.route('/:catID/products')
		.get((req, res, next) => {
			return Products
				.find({ categoryID: req.params.catID })
				.sort({ name: +1 })
				.exec()
				.then(data => hr.ok(data).end(res))
				.catch(next);
		})

module.exports = router;
