'use strict';

const http = require('http');
const cluster = require('cluster');

const Promise = require('bluebird');
global.Promise = Promise;
const express = require('express');
const numCPUs = require('os').cpus().length;


const app = express();
const HOST = 'localhost';
const PORT = process.env.PORT || 3000;

app.use('/', require('./config/express'));

if(cluster.isMaster) {
	console.log(`[cluster] Master ${process.pid} is running.`);
	for(let i = 0; i < numCPUs; i++) cluster.fork();
	cluster.on('exit', (worker, code, signal) =>
		console.warn(`[cluster] worker ${worker.process.pid} died.`));
}
else {
	http
		.createServer(app)
		.listen(PORT, HOST, () =>
			console.log(`[Express] The server is now running on http://${HOST}:${PORT}.`))
		.on('error', function(err) {
			console.error(`[Express] Connection error: ${err}.`);
			this.close(() => {
				console.error(`[Express] The connection has been closed.`);
				process.exit(-2);
			});
		});
	console.log(`[cluster] worker ${process.pid} started.`);
}
