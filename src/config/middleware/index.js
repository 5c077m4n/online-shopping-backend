'use strict';

const path = require('path');

const jwt = require('jsonwebtoken');
const fs = require('fs-extra');
const hr = require('http-responder');
const to = require('await-fn');

const User = require('mongoose').model('User');


const decodeToken = (req, res, next) => {
	const token = req.headers['x-access-token'];
	if(!token) throw hr.forbidden('No token provided.');
	else return fs.readFile(path.join(__dirname, '../../utils/key.pub'), 'utf8')
		.then(async publicKey => {
			const [err1, decoded] = await to(
				jwt.verify, { params: [
					token,
					publicKey,
					{ expiresIn: '12h', algorithms: ['RS256'] }
				] }
			);
			if(err1) return Promise.reject(err1);
			const [err2, user] = await to(
				User
					.findById(decoded.user._id)
					.exec()
			);
			if(err2) return Promise.reject(err2);
			if(!user) return Promise.reject(
				hr.unauthorized('There was an error in decoding your token.')
			);
			return user;
		})
		.catch(Promise.reject);
};

module.exports.isManager = (req, res, next) => {
	decodeToken(req, res, next)
		.then(user => (user.isManager)?
			next() : next(hr.unauthorized())
		)
		.catch(next);
};

module.exports.verifyToken = (req, res, next) => {
	decodeToken(req, res, next)
		.then(user => (user)?
			next() : next(hr.unauthorized())
		)
		.catch(next);
};

module.exports.verifyUser = (req, res, next) => {
	decodeToken(req, res, next)
		.then(user => (req.params.username === user.username)?
			next() : next(hr.unauthorized())
		)
		.catch(next);
};
