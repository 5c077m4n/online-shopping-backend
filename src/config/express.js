'use strict';

const path = require('path');

const express = require('express');
const fs = require('fs-extra');
const logger = require('morgan');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compress = require('compression');
const cors = require('cors');
const hr = require('http-responder');
const router = require('express').Router();
const mongoose = require('mongoose');
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
require('../models'); /** Load all mongoose models before use */

const middleware = require('./middleware');


const dbURI = 'mongodb://127.0.0.1:27017/online-shopping';
const logPath = path.join(__dirname, '../utils/server-stream.log');

fs.ensureFile(logPath)
	.then(() => fs.createWriteStream(logPath, { flags: 'a' }))
	.then(accessLogStream => {
		router.use(logger('dev'));
		router.use(logger('dev', { stream: accessLogStream }));
		return accessLogStream;
	})
	.then(accessLogStream => {
		mongoose.set('debug', (collectionName, methodName, input, info) => {
			const strOut =
				`[Mongoose] ${collectionName}.${methodName}(${JSON.stringify(input)}) ` +
				`(${JSON.stringify(info)}) @${new Date().toLocaleString()}`;
			console.log(strOut);
			accessLogStream.write(strOut + '\n');
		});
	})
	.catch(err => {
		console.warn(err);
		console.warn('Logging is DISABLED because the log file could not be created.');
	});

router.use(helmet());
router.options('*', cors());
router.use(cors());

mongoose.connect(dbURI, { useNewUrlParser: true })
	.then(() => console.log('[Mongoose] You have been successfully connected to the database.'))
	.catch(err => console.error(`[Mongoose] Connection error: ${err}`));
mongoose.connection
	.on('error', err => console.error(`[Mongoose] Connection error: ${err}`));

router.use((req, res, next) => {
	req.connection.setNoDelay(true);
	next();
});
router.use(
	compress({
		filter: (req, res) => (req.headers['x-no-compression'])? false : compress.filter(req, res),
		level: 6
	})
);

router.use(bodyParser.json());
router.use('/', require('./routes'));
router.use('/statistics', require('./routes/statistics-routes'));
router.use('/customers', middleware.verifyToken, require('../controllers/customer-routes'));
router.use(
	'/managers',
	middleware.verifyToken,
	middleware.isManager,
	require('../controllers/manager-routes')
);
router.use('/products', middleware.verifyToken, require('../controllers/product-routes'));
router.use('/categories', middleware.verifyToken, require('../controllers/category-routes'));
router.use(express.static('public'));

router.use((req, res, next) => next(hr.notFound('The requested page cannot be found.')));
router.use((error, req, res, next) => {
	if(!hr.isHR(error)) error = hr.improve(error);
	console.error(`[Express] ${error.stack}`);
	return error.end(res);
});

module.exports = router;
