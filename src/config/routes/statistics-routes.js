'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');

const Product = mongoose.model('Product');
const Order = mongoose.model('Order');


router.route('/count-products')
	.get((req, res, next) => {
		return Product
			.countDocuments({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/count-orders')
	.get((req, res, next) => {
		return Order
			.countDocuments({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

module.exports = router;
