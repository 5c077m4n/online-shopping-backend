'use strict';

const path = require('path');

const router = require('express').Router();
const jwt = require('jsonwebtoken');
const fs = require('fs-extra');
const hr = require('http-responder');
const to = require('await-fn');
const upload = require('multer')({ dest: 'public/images/' });

const middelware = require('../middleware');
const User = require('mongoose').model('User');


const logPath = path.join(__dirname, '../../utils/client-stream.log');
const publicImagesPath = path.join(__dirname, '../../../public/images/uploads');
const privateKeyPath = path.join(__dirname, '../../utils/key');
const publicKeyPath = path.join(__dirname, '../../utils/key');

const createUserToken = user => {
	return fs.readFile(privateKeyPath, 'utf8')
		.then(async privateKey => {
			const [err, token] = await to(jwt.sign, {
				params: [
					{ user },
					{ key: privateKey, passphrase: 'qwerty_123' },
					{ expiresIn: '12h', algorithm: 'RS256' }
				]
			});
			if(err) return Promise.reject(err);
			if(!token) return Promise.reject(
				hr.internalServerError('The token was not created.')
			);
			return token;
		})
		.catch(Promise.reject);
};

router.route('/')
	.all((req, res, next) => hr.ok('Great success!').end(res));

fs.ensureFile(logPath)
	.then(() => {
		router.route('/log')
			.get((req, res, next) => {
				return fs.readFile(logPath)
					.then(content => hr.ok(undefined, content).end(res))
					.catch(next);
			})
			.post((req, res, next) => {
				return fs.appendFile(logPath, `${req.body.log}\n`)
					.then(() => hr.created(`Log added.`).end(res))
					.catch(next);
			})
			.delete((req, res, next) => {
				return fs.truncate(logPath, 0)
					.then(() => hr.noContent('Log cleared.').end(res))
					.catch(next);
			});
	})
	.catch(err => {
		router.route('/logs')
			.all((req, res, next) => {
				return hr
					.internalServerError(`The log file could not be created: ${err}`)
					.end(res);
			});
	});

router.route('/login')
	.post((req, res, next) => {
		User.authenticate(req.body.username, req.body.password, next)
			.then(createUserToken)
			.then(token => hr.ok({ token }).end(res))
			.catch(next);
	});

router.route('/register')
	.post((req, res, next) => {
		new User(req.body)
			.save()
			.then(user =>
				(user)? user : Promise.reject(
					hr.internalServerError('The user has NOT been created.')
				)
			)
			.then(createUserToken)
			.then(token => hr.created({ token }).end(res))
			.catch(next);
	});

router.route('/public-key')
	.get(middelware.verifyToken, async (req, res, next) => {
		return fs.readFile(publicKeyPath, 'utf8')
			.then(publicKey => hr.ok({ publicKey }).end(res))
			.catch(next);
	});

fs.ensureDir(publicImagesPath)
	.then(() => {
		return router.route('/upload-image')
			.post(
				middelware.verifyToken,
				middelware.isManager,
				upload.single('imageFile'),
				(req, res, next) => {
					if(req.file)
						return hr.created(`${req.file.originalname} uploaded successfully.`, req.file).end(res);
					return hr.internalServerError('The image was not recieved.').end(res);
				}
			);
	})
	.catch(err => {
		return router.route('/upload-image')
			.all((req, res, next) => hr.internalServerError(err).end(res));
	});

module.exports = router;
